#!/bin/bash
clear
options="Schreiben Korrekturschreiben"
echo "Was möchten sie tun?"
select option in $options; do
  if [ Korrekturschreiben = $option ]; then
    clear
    cd Schreibtisch/'New Generation'
    echo "Viel Erfolg!"
    lowriter "New Generation Alt 1.odt" & disown
    lowriter "New Generation.odt" & disown
    exit
  elif [ Schreiben = $option ]; then
    clear
    cd Schreibtisch/'New Generation'
    echo "Viel Erfolg!"
    lowriter "New Generation.odt" & disown
    exit
  fi
done
